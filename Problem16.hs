import Data.Char

main = putStrLn(show result)
result = sum power_digits
  where power = show (2^1000)
        power_digits = map digitToInt power