prevFrac :: Int -> Int -> Int -> (Int, Int)
prevFrac num den limit = inner 1 limit 1 where
    inner numerator denominator i 
                | i == limit = (numerator, denominator)
                | den * m < num * i && m * denominator > numerator * i = inner m i (i + 1)
                | otherwise = inner numerator denominator (i + 1)

                where m = num * i `quot` den

main :: IO ()
main = print $ fst $ prevFrac 3 7 1000000