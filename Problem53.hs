import Data.Char

import System.IO


factorial 0 = 1

factorial n | n > 0 = n * factorial(n - 1)


combination(r, n) = factorial n `div` (factorial r * factorial (n - r))


value = [(n, x) | x <- [1..100], n <- [1..x]]

result = length $ filter (>1000000) $ map combination (value)


main= do

   print(result)
