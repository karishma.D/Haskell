main = putStrLn(show fib)

fib = sum(filter even(takeWhile(<= 4000000) fibonacci))

fibonacci = 0: 1: (zipWith(+) fibonacci(tail fibonacci))

