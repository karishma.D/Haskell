import Data.Char (digitToInt)
 
digitSum :: Integer -> Int
digitSum = sum . map digitToInt . show
 
maxDigitSum :: Int
maxDigitSum = maximum $ map digitSum [a^b | a <- [1..100], b <- [1..100]]

main :: IO()
main = print $ maxDigitSum