isFactor n x = rem n x == 0
isqrt = floor.sqrt.fromIntegral
isComposite n = or $ map(isFactor n)[2..isqrt n]
isPrime = not.isComposite
primeFactors n = [x | x <- [2.. isqrt n], rem n x == 0, isPrime x]
main = putStrLn(show result)
result = primeFactors(600851475143 :: Integer)