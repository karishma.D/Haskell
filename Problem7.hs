isqrt = floor.sqrt.fromIntegral

isFactor n x = rem n x == 0
isComposite n = or $ map(isFactor n)[2..isqrt n]
isPrime = not.isComposite
primes = 2: [x | x <- [2..], isPrime(x)]

result = primes !! 10001

main = do putStrLn $ show result